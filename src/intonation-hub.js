import { LitElement, html, css } from 'lit-element';
import './components/frequency-visualizer';
import './components/frequency-input';

export class IntonationHub extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      page: { type: String },
    };
  }

  static get styles() {
    return css`
      :host {
        min-height: 100vh;
      }
    `;
  }

  constructor() {
      super();
      const AudioContext = window.AudioContext || window.webkitAudioContext;
      let audioCtx = new AudioContext();
      let osc = audioCtx.createOscillator();
        osc.frequency.value = 440;
        osc.type= 'sawtooth'
        osc.connect(audioCtx.destination);
        osc.start();

        document.globalAudioContext = audioCtx;

  }

  render() {
    return html`
      <main>
        Hello!
        <frequency-input></frequency-input>
      </main>  
    `;
  }
}


customElements.define('intonation-hub', IntonationHub);
