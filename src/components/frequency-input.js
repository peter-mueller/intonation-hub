import { LitElement, html, css } from 'lit-element';
import './frequency-visualizer'


export class FrequencyInput extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      max: { type: Number},
      min: { type: Number},
      frequency: {type: Number, }
    };
  }


  getFrequencyRange() {
      return this.max - this.min;
  }

  constructor() {
      super();

      this.min = 400;
      this.max = 500;
      this.frequency = 440;
      this.audioCtx = document.globalAudioContext;

  }

  static get styles() {
    return css`
    `;
  }

  render() {
    if (this.frequency && this.osc) {
        this.osc.frequency.value = this.frequency;
    }
    return html`
        <div id="visualizer" 
            @mousemove=${(e) => {
                if(e.buttons == 0) {
                    return;
                }
                const freq = this.min + this.getFrequencyRange() * (e.offsetY / 256);
                this.frequency = Math.min(this.max, Math.max(this.min,freq));
            }}

            @touchmove=${(e) => {
                let dim = this.getBoundingClientRect();
                const freq = this.min + this.getFrequencyRange() * ((e.touches[0].clientY - dim.top)/ 256);
                this.frequency = Math.min(this.max, Math.max(this.min,freq));
            }}

            @mousedown=${() => this.startOsc()}
            @touchstart=${() => this.startOsc()}

            @mouseup=${() => this.stopOsc()}
            @mouseleave=${() => this.stopOsc()}
            @touchend=${() => this.stopOsc()}

        >
            <frequency-visualizer frequency=${this.frequency} min=${this.min} max=${this.max}></frequency-visualizer>
        </div>
    `;
  }

  startOsc() {
    let osc = this.audioCtx.createOscillator();
    osc.type = 'sawtooth'
    osc.frequency.value = this.frequency;
    osc.connect(this.audioCtx.destination);
    this.osc = osc;
    this.osc.start();
  }

  stopOsc() {
      if (!this.osc) {
          return;
      }
        this.osc.stop(this.audioCtx.currentTime);
        this.osc = null;
  }
}


customElements.define('frequency-input', FrequencyInput);
