import { LitElement, html, css } from 'lit-element';
import {styleMap} from 'lit-html/directives/style-map.js';

export class FrequencyVisualizer extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      max: { type: Number},
      min: { type: Number},
      frequency: {type: Number}
    };
  }

  constructor() {
      super();
      this.min = 0;
      this.max = 100;
      this.frequency = 50;
  }

  static get styles() {
    return css`
        #box {
            border: 2px solid black;
            height: 256px;
            max-width: 512px;
        }

        #bar {
            bottom: 0;
            border: 1px solid black;
            margin: 0;
            transform: translateY(0px);
        }
    `;
  }

  render() {
    var percentage = (this.frequency - this.min) / (this.max - this.min);
    var styles = {
        transform: `translateY(${(percentage * 256)-1}px)`
    }

    return html`
      <div id="box">
          <hr id="bar" style=${styleMap(styles)}>
      </div>
    `;
  }
}


customElements.define('frequency-visualizer', FrequencyVisualizer);
